package com.ynov.parisport.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.ynov.parisport.Model.Match;

@Repository
public interface MatchRepository extends JpaRepository <Match, Long>{
		
	@Query("FROM Match WHERE ID = :ID and date = :date and time = :time and event = :event and team1 = :team1 and team2 = :team2 and team1cote = :team1cote and team2cote = :team2cote and isfinished = :isfinished and result = :result")
    Match getAll(@Param("ID") int ID,@Param("date") String date, @Param("time") String time,@Param("event") String event, @Param("team1") String team1, @Param("team2") String team2, @Param("team1cote") float team1cote, @Param("team2cote") float team2cote, @Param("isfinished") boolean isfinished,@Param("result") String result);
	
	
}
