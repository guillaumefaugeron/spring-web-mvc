package com.ynov.parisport.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ynov.parisport.Model.Bet;

@Repository
public interface BetRepository extends JpaRepository <Bet, Long>{
	
}
