package com.ynov.parisport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParisportApplication {

    public static void main(String[] args) {
        SpringApplication.run(ParisportApplication.class, args);
        
    }
}