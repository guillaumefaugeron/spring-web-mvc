package com.ynov.parisport.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "matches")
public class Match {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;
	
	private String date;
	private String time;
	private String event;
	private String team1;
	private String team2;
	private float team1cote;
	private float team2cote;
	private boolean isfinished;
	private String result;
	
	public Match(long ID, String date,String time, String event, String team1, String team2, float team1cote, float team2cote, boolean isfinished,String result) {
		this.ID = ID;
		this.date = date;
		this.time = time;
		this.event = event;
		this.team1 = team1;
		this.team2 = team2;
		this.team1cote = team1cote;
		this.team2cote = team2cote;
		this.isfinished = isfinished;
		this.result = result;
	}
	
	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getTeam1() {
		return team1;
	}

	public void setTeam1(String team1) {
		this.team1 = team1;
	}

	public String getTeam2() {
		return team2;
	}

	public void setTeam2(String team2) {
		this.team2 = team2;
	}

	public float getTeam1cote() {
		return team1cote;
	}

	public void setTeam1cote(float team1cote) {
		this.team1cote = team1cote;
	}

	public float getTeam2cote() {
		return team2cote;
	}

	public void setTeam2cote(float team2cote) {
		this.team2cote = team2cote;
	}

	public boolean isIsfinished() {
		return isfinished;
	}

	public void setIsfinished(boolean isfinished) {
		this.isfinished = isfinished;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Match() {
		
	}
	

		
}
