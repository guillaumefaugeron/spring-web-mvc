package com.ynov.parisport.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "userbets")
public class Bet {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ID; 
	private int point_pari;
	private int id_matches;
	private String team;
	private String username;
	private float cote;
	
	public Bet(int point_pari, int id_matches, String team, String username, float cote) {
		this.point_pari = point_pari;
		this.id_matches = id_matches;
		this.team = team;
		this.username = username;
		this.cote = cote;
		
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public int getPoint_pari() {
		return point_pari;
	}

	public void setPoint_pari(int point_pari) {
		this.point_pari = point_pari;
	}

	public int getId_matches() {
		return id_matches;
	}

	public void setId_matches(int id_matches) {
		this.id_matches = id_matches;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public float getCote() {
		return cote;
	}

	public void setCote(float cote) {
		this.cote = cote;
	}

}
