package com.ynov.parisport.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ynov.parisport.Repository.MatchRepository;

@RestController
@Controller
public class MatchController {
	
	private MatchRepository repository;
    MatchController(MatchRepository matchRepository){
        this.repository = matchRepository;
    }
	
    @RequestMapping(value= {"/afficherMatchs"}, method = RequestMethod.GET)
    public ModelAndView afficherMatchs(Model model){
    	
        return new ModelAndView("afficherMatchs", "Match", model.addAttribute("matchs", repository.findAll()));
        
    }
	
	@RequestMapping(value = "/afficherMatch", method = RequestMethod.GET)
	public  ModelAndView afficherMatch(@RequestParam String ID, Model model){
		 	 
        return new ModelAndView("afficherMatch", "Match", model.addAttribute("match", repository.findById(Long.parseLong(ID)).get() ) );
	}
	
}
