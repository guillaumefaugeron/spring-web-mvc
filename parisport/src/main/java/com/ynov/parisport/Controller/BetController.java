package com.ynov.parisport.Controller;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ynov.parisport.Model.Bet;
import com.ynov.parisport.Model.Match;
import com.ynov.parisport.Model.User;
import com.ynov.parisport.Repository.BetRepository;
import com.ynov.parisport.Repository.MatchRepository;
import com.ynov.parisport.Repository.UserRepository;

@Controller
public class BetController {

	private UserRepository userRepository;
	private MatchRepository matchRepository;
	private BetRepository betRepository;
	
    BetController(UserRepository userRepository, MatchRepository matchRepository, BetRepository betRepository){
        this.userRepository = userRepository;
        this.matchRepository = matchRepository;
        this.betRepository = betRepository;
    }
   

	@RequestMapping(value="/pari", method = RequestMethod.GET)
	  public ModelAndView printUser(ModelMap model, @RequestParam String ID) {
	
	      return new ModelAndView("pari", "Match", model.addAttribute("match", matchRepository.findById(Long.parseLong(ID)).get() ) );
	}
	
	@RequestMapping(value="/team1", method = RequestMethod.POST)
	public String pariTeam1(Model model, @RequestParam int ID) {
	    
		  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	      String username = auth.getName();
	      User user = this.userRepository.findByUsername(username);
		  Match match = this.matchRepository.findById((long) ID).get();
	      float newPoints = user.getPoints() - 100;
	      user.setPoints(newPoints);
	     
	     this.betRepository.save(new Bet(100, (int) match.getID(), match.getTeam1(), user.getUsername(), match.getTeam1cote()));
	     
	     return "redirect:/";
	}
	
	@RequestMapping(value="/team2", method = RequestMethod.POST)
	public String pariTeam2(Model model, @RequestParam int ID) {
	    
		  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	      String username = auth.getName();
	      User user = this.userRepository.findByUsername(username);
	      if(user.getPoints()>100) {
	    	  System.out.println(user.getPoints());
	 	      float newPoints = user.getPoints() - 100;
		      user.setPoints(newPoints);
		      this.userRepository.save(user);
			  Match match = this.matchRepository.findById((long) ID).get();
			  this.betRepository.save(new Bet(100, (int) match.getID(), match.getTeam2(), user.getUsername(), match.getTeam2cote()));
			  return "redirect:/";
	      } 
	      else {
	    	  return "redirect:/";
	      }
	     
	     
	}
}
