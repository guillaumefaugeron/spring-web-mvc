package com.ynov.parisport.Controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ynov.parisport.Form.UserForm;
import com.ynov.parisport.Model.User;
import com.ynov.parisport.Repository.UserRepository;

@Controller
public class UserController {
	
	private UserRepository userRepository;
	
	@Value("${error.message.pseudo}")
	private String errorMessagePseudo;
	
	@Value("${error.message.champs.manquants}")
	private String errorMessageChampsManquants;
	
	
	UserController(UserRepository userRepository){
		this.userRepository = userRepository;
	}
	
	
	@RequestMapping(value = {"/creerCompte" }, method = RequestMethod.GET)
	public String createUser(Model model) {
		UserForm userForm = new UserForm();
		model.addAttribute("userForm", userForm);
		return "creerCompte";
	}
	

	@RequestMapping(value = {"/monCompte" }, method = RequestMethod.GET)
	public ModelAndView myAccount(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		User user = this.userRepository.findByUsername(username);
		model.addAttribute("user", user );
		return new ModelAndView("monCompte", "User", model);	
	}

	
	@RequestMapping(value = {"/creerCompte" }, method = RequestMethod.POST)
	public String createUser(Model model,
			@ModelAttribute("UserForm") UserForm userForm
			) {
		String name = userForm.getName();
		String surname = userForm.getSurname();
		String username = userForm.getUsername();
		String email = userForm.getEmail();
		String password = userForm.getPassword();
		String role = "user";
		
		if (!username.isEmpty() && this.userRepository.findByUsername(username) != null) {
			model.addAttribute("errorMessagePseudo", errorMessagePseudo);
			model.addAttribute("userForm", userForm);
			return "creerCompte";
		}
		
		if (   !name.isEmpty()
			&& !surname.isEmpty()
			&& !username.isEmpty()
			&& !email.isEmpty()
			&& !password.isEmpty()
			&& !role.isEmpty()) {
			
			this.userRepository.save(new User(name, surname, username, email, password, role));
			return "redirect:/login";
		}

		model.addAttribute("errorMessageChampsManquants", errorMessageChampsManquants);
		model.addAttribute("userForm", userForm);
		return "creerCompte";
	}
	
	
	@RequestMapping(value = {"/modifierCompte" }, method = RequestMethod.GET)
	public String modifyUser(Model model) {
		UserForm userForm = new UserForm();
		model.addAttribute("userForm", userForm);
		return "modifierCompte";
	}
	
	
	@RequestMapping(value = {"/modifierCompte" }, method = RequestMethod.POST)
	public String modifierCompte(Model model,
			@ModelAttribute("userForm") UserForm userForm
			) {
		
		String name = userForm.getName();
		String surname = userForm.getSurname();
		String email = userForm.getEmail();
		String password = userForm.getPassword();
		
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User user = this.userRepository.findByUsername(username);
	    if (!name.isEmpty()) {
	    	user.setName(name);
	    }
	    
	    if (!surname.isEmpty()) {
	    	user.setSurname(surname);
	    }
	    
	    if (!email.isEmpty()) {
	    	user.setEmail(email);
	    }
	    
	    if (!password.isEmpty()) {
	    	user.setPassword(password);
	    }
	    	    
		this.userRepository.save(user);
		return "redirect:/monCompte";
		
	}
}
