CREATE DATABASE parisport;
use parisport



create table matches
(
    ID        int(255)    not null
        primary key,
    date       varchar(50) null,
    time       varchar(50) null,
    event      varchar(50) null,
    team1      varchar(50) null,
    team2      varchar(50) null,
    team1Cote  float       null,
    team2Cote  float       null,
    isfinished boolean     null,
    result     varchar(50) null
);

INSERT INTO parisport.matches (ID, date, time, event, team1, team2, team1cote, team2cote, isfinished, result) VALUES (856356, '2019-11-27', '08:58:00', 'ESL-One', 'Vitality', 'Faze', '2.5', '1.2', 0, null);
INSERT INTO parisport.matches (ID, date, time, event, team1, team2, team1cote, team2cote, isfinished, result) VALUES (2012351, '2019-11-24', '12:00:00', 'China-League', 'Astralis', 'Tyloo', '1.1', '9.8', 1, '2-0');
INSERT INTO parisport.matches (ID, Date, Time, Event, team1, team2, team1cote, team2cote, isfinished, result) VALUES (5684227, '2019-11-28', '10:00:00', 'ESL', 'Solary', 'G2', '10', '1.1', 0, null);




create table user
(
    ID       int auto_increment
        primary key,
    username   varchar(50) not null,
    name     varchar(50) not null,
    surname  varchar(50) not null,
    email    varchar(50) not null,
    password varchar(1000) not null,
    points   float       null,
    role varchar(50) not null,
    account_non_expired boolean not null,
    account_non_locked boolean not null,
    credentials_non_expired boolean not null,
    enabled boolean not null
);


create table userbets
(
    ID        int auto_increment
        primary key,
    point_pari    int          not null,
    id_matches    int         not null,
    team         varchar(50) not null,
    username     varchar(50) not null,
    cote         float       not null
);

create table result
(
    ID        int           not null,
    result    varchar(50)   null
);