import requests
# import datetime
from MySQLdb.constants.FIELD_TYPE import NULL
from bs4 import BeautifulSoup
import MySQLdb


def update():
    db = MySQLdb.connect(host="localhost", user="root", passwd="25Guillaume25", db="parisport")
    cursor = db.cursor()
    try:
        cursor.execute("SHOW TABLES;")
        results = cursor.fetchall()
        print(results)
    except:
        print("ERROR IN CONNECTION")

    cursor.execute("""
        update matches 
        SET isfinished =1
        where ID IN (SELECT ID
                     FROM result)""")
    db.commit()

def resultinsert(data):
    db = MySQLdb.connect(host="localhost", user="root", passwd="25Guillaume25", db="parisport")
    cursor = db.cursor()
    try:
        cursor.execute("SHOW TABLES;")
        results = cursor.fetchall()
        print(results)
    except:
        print("ERROR IN CONNECTION")

    cursor.executemany("""
        INSERT INTO result (ID,result)
        VALUES (%(ID)s , %(result)s) ;""", data)
    db.commit()


def get_results():
    results = BeautifulSoup(requests.get(
        "http://www.hltv.org/results",
         headers={"referer": "https://www.hltv.org/stats",
          "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"}).text,
                            "lxml")

    holderresults = results.find("div", {"class": "results-all"})
    con = holderresults.find_all_next("div", {"class": "result-con"})
    resultList=[]
    for con1 in con :
        winerteam=con1.find('div', {"team team-won"}).string
        if winerteam == con1.find('div', {"class": "line-align team1"}).find("img").get("alt"):
            loserteam=con1.find('div', {"class": "line-align team2"}).find("img").get("alt")
        else:
            loserteam=con1.find('div', {"class": "line-align team1"}).find("img").get("alt")

        id = con1.find("a").get("href").split('/')[2]
        result = winerteam+"-"+loserteam + "_" + con1.find('span', {"score-won"}).string +"-"+con1.find('span', {"score-lost"}).string

        results = {'ID': id, 'result': result}
        resultList.append(results)

    return resultList


print(get_results())
resultinsert(get_results())

update()